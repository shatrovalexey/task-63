<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
	var $tabs = [
		'metro' , 'transport' , 'billboard' ,
	] ;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
	$current_tab = Yii::$app->request->get( 'tab' ) ;

	if ( ! in_array( $current_tab , $this->tabs ) ) {
		$current_tab = $this->tabs[ 0 ] ;
	}

	$results = Yii::$app->db->createCommand( "
SELECT
	`t1`.*
FROM
	`{$current_tab}` AS `t1` ;
	" )->queryAll( ) ;

        return $this->render( 'index' , [
		'results' => $results ,
		'current_tab' => $current_tab ,
		'tabs' => $this->tabs ,
	] ) ;
    }

	/**
	* Выбранные пункты в таблицах
	*/
	public function actionBin( ) {
		$sql = [ ] ;
		$results = [ ] ;

		foreach ( $this->tabs as $tab ) {
			$results[ $tab ] = Yii::$app->db->createCommand( "
SELECT
	`t1`.`id` ,
	`t1`.`name`
FROM
	`{$tab}` AS `t1`
WHERE
	( `t1`.`active` > 0 ) ;
			" )->queryAll( ) ;
		}

		return $this->render( 'bin' , [
			'results' => $results ,
		] ) ;
	}

	/**
	* Указать активность записи в таблице
	*
	* @method POST
	* - @param integer $id - идентификатор записи
	* - @param string $tab - название таблицы
	* - @param boolean $active - активность
	*
	* @throws \yii\web\BadRequestHttpException
	*/
	public function actionSetActiveTab( ) {
		$data = Yii::$app->request->post( ) ;

		if ( empty( $data ) ) {
			throw new \yii\web\BadRequestHttpException( 'неверный тип запроса' ) ;
		}
		if ( empty( $data[ 'tab' ] ) ) {
			throw new \yii\web\BadRequestHttpException( 'не указана таблица' ) ;
		}
		if ( ! in_array( $data[ 'tab' ] , $this->tabs ) ) {
			throw new \yii\web\BadRequestHttpException( 'таблица указана не верно' ) ;
		}
		if ( empty( $data[ 'id' ] ) ) {
			throw new \yii\web\BadRequestHttpException( 'не указан идентификатор записи' ) ;
		}
		if ( ! isset( $data[ 'active' ] ) ) {
			throw new \yii\web\BadRequestHttpException( 'не указан параметр активности' ) ;
		}

		$dbh = Yii::$app->db->masterPdo ;
		$sth = $dbh->prepare( "
UPDATE
	`{$data[ 'tab' ]}` AS `t1`
SET
	`t1`.`active` := :active
WHERE
	( `t1`.`id` = :id ) ;
		" ) ;
		$result = $sth->execute( [
			':active' => $data[ 'active' ] ,
			':id' => $data[ 'id' ] ,
		] ) ;
		$sth->closeCursor( ) ;

		return $this->asJson( [
			'result' => $result ,
		] ) ;
	}

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
