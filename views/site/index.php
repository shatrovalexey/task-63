<?php

/* @var $this yii\web\View */

$this->title = 'task-63';

if ( $tabs ) {
?>
<ul>
	<?php foreach ( $tabs as $tab ) { ?>
	<li>
		<?php if ( $current_tab == $tab ) { ?>
		<b><?=htmlspecialchars( $tab )?></b>
		<?php } else { ?>
		<a href="/?tab=<?=htmlspecialchars( $tab )?>"><?=htmlspecialchars( $tab )?></a>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php } ?>
<?php if ( $results ) { ?>
<form action="/site/set-active-tab" method="post">
	<input name="tab" value="<?=htmlspecialchars( $current_tab )?>" type="hidden">
	<ul>
		<?php foreach ( $results as $result ) { ?>
		<li>
			<label>
				<input type="checkbox" name="id" value="<?=$result[ 'id' ]?>"<?php
					if ( $result[ 'active' ] ) { ?> checked<?php }
				?> class="ctrl-tab-item">
				<span><?=htmlspecialchars( $result[ 'name' ] )?></span>
			</label>
		</li>
		<?php } ?>
	</ul>
</form>
<?php } ?>