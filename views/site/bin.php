<?php foreach ( $results as $key => $result ) { ?>
<h2><?=htmlspecialchars( $key )?></h2>
<ul>
	<?php foreach ( $result as $item ) { ?>
	<li><?=htmlspecialchars( $item[ 'name' ] )?></li>
	<?php } ?>
</ul>
<?php } ?>