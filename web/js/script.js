jQuery( function( ) {
	jQuery( ".ctrl-tab-item" ).on( "change" , function( ) {
		let $self = jQuery( this ) ;
		let $form = $self.parents( "form:first" ) ;

		jQuery.ajax( {
			"url" : $form.attr( "action" ) ,
			"type" : $form.attr( "method" ) ,
			"data" : {
				"id" : $self.val( ) ,
				"tab" : $form.find( "input[name=tab]" ).val( ) ,
				"active" : $self.prop( "checked" ) ? 1 : 0
			}
		} ) ;
	} ) ;
} ) ;