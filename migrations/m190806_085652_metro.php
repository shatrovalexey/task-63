<?php

use yii\db\Migration;

/**
 * Class m190806_085652_metro
 */
class m190806_085652_metro extends Migration
{
	var $table_name = 'metro' ;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	$dbh = Yii::$app->db->masterPdo ;

	$dbh->exec( "
CREATE TABLE IF NOT EXISTS `{$this->table_name}`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
	`name` CHAR( 40 ) NOT null COMMENT 'название' ,
	`active` TINYINT( 1 ) UNSIGNED NOT null DEFAULT true COMMENT 'активность' ,

	PRIMARY KEY( `id` )
) COMMENT 'биллборд' ;
	" ) ;

	$dbh->exec( "
SET @@session.auto_increment_increment = 10 ;
	" ) ;

	$dbh->exec( "
INSERT INTO
	`{$this->table_name}`( `name` )
VALUES
	( '{$this->table_name}-1' ) ,
	( '{$this->table_name}-2' ) ,
	( '{$this->table_name}-3' ) ;
	" ) ;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	$dbh = Yii::$app->db->masterPdo ;

	$dbh->exec( "
DROP TABLE IF EXISTS `{$this->table_name}` ;
	" ) ;

	$dbh->exec( "
SET @@session.auto_increment_increment = 1 ;
	" ) ;
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190806_085652_metro cannot be reverted.\n";

        return false;
    }
    */
}
